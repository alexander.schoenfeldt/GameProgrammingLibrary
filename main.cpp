//
//  main.cpp
//  GameProgrammingLibrary
//
//  Created by Alexander Schoenfeldt on 18/08/16.
//  Copyright © 2016 Alexander Schoenfeldt. All rights reserved.
//
//  Test class to demonstrate the functionality of the library

#include <iostream>
#include "FHighscoreList.h"

int main(int argc, const char * argv[]) {
   
    //Create highscore List
    FHighscoreList HighscoreList;
    
    //Create scores
    FScore score1 = {"Alex",2000};
    FScore score3 ("Yanik",1000);
    FScore score2 = {"Tobi",500};
    
    //Add scores to List
    HighscoreList.AddScore(score1);
    HighscoreList.AddScore(score2);
    HighscoreList.AddScore(score3);
    
    //Print out all scores of HighscoreList
    for(int i = 0; i< HighscoreList.GetHighscoreList().size(); i++)
    {
        std::cout << i+1 << ". " << HighscoreList.GetHighscoreList()[i].GetName() << ", Score: "<< HighscoreList.GetHighscoreList()[i].GetScore() <<std::endl;
    }
    
    //Print out a specific score
    std::cout << HighscoreList.GetHighscoreEntry(2).GetName() << ", Score: " << HighscoreList.GetHighscoreEntry(2).GetScore() << std::endl;
    
    //Print out an invalid score
    std::cout << HighscoreList.GetHighscoreEntry(10).GetName() << ", Score: " << HighscoreList.GetHighscoreEntry(10).GetScore() << std::endl;
    
    //Print out an invalid score
    std::cout << HighscoreList.GetHighscoreEntry(-1).GetName() << ", Score: " << HighscoreList.GetHighscoreEntry(-1).GetScore() << std::endl;
    
    //Save the current HighscoreList
    HighscoreList.SaveHighscoreList();

    //Load another HighscoreList
    FHighscoreList HighscoreList2;
    
    //Put the Storage2.txt file from the GameProgrammingLibrary Folder to the folder where the GameProgrammingLibrary executable is located. Path can be found in Folder Products -> GameProgrammingLibrary and is shown in the inspector.
    HighscoreList2 = HighscoreList2.LoadHighscoreList("Storage2.txt");
    
    std::cout << "Size of List 2: " << HighscoreList2.GetHighscoreList().size() << std::endl;
    
    //Print out all scores of HighscoreList2
    for(int i = 0; i< HighscoreList2.GetHighscoreList().size(); i++)
    {
        std::cout << i+1 << ". " << HighscoreList2.GetHighscoreList()[i].GetName() << ", Score: "<< HighscoreList2.GetHighscoreList()[i].GetScore() <<std::endl;
    }
    
    return 0;
}
