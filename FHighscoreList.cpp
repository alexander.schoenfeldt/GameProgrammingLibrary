//
//  FHighscoreList.cpp
//  GameProgrammingLibrary
//
//  Created by Alexander Schoenfeldt on 20/08/16.
//  Copyright © 2016 Alexander Schoenfeldt. All rights reserved.
//
// This class provides a basic data structure to implement a score system in a game.

#include "FHighscoreList.h"

std::vector<FScore> FHighscoreList::GetHighscoreList()
{
    return HighscoreList;
}

// If Index is out of bounds, an invalid score entry will be returned
FScore FHighscoreList::GetHighscoreEntry(int32_t Index)
{
    if(Index<HighscoreList.size() && Index>-1)
    {
        return HighscoreList[Index];
    } else
    {
        FScore invalid = {"Invalid Index", -1};
        return invalid;
    }
}

// Also sorts the score into the right position
void FHighscoreList::AddScore(FScore Score)
{
    HighscoreList.insert(HighscoreList.end(), Score);
    SortHighscoreList(&HighscoreList);
    return;
}

//Sorts in descending order
void FHighscoreList::SortHighscoreList(std::vector<FScore> *HighscoreList) {
    std::sort(HighscoreList->rbegin(), HighscoreList->rend());
}

// Saves the current HighscoreList to a file
void FHighscoreList::SaveHighscoreList()
{
    std::ofstream Storage;
    Storage.open("Storage.txt");
    
    if(Storage.is_open())
    {
        for(int i = 0; i< HighscoreList.size(); i++)
        {
        Storage << i+1 << ". " << HighscoreList[i].GetName() << ", Score: "<< HighscoreList[i].GetScore() <<std::endl;
        }
    } else
    {
        std::cout<< "No file has been created!\n";
    }
    
    Storage.close();
    return;
}


FHighscoreList FHighscoreList::LoadHighscoreList(std::string Path)
{
    std::ifstream Storage;
    Storage.open(Path);
    
    std::string Line = "";
    FHighscoreList LoadedList;
    if(Storage.is_open())
    {
        while ( std::getline(Storage,Line) )
        {
            // Parse name from Line
            const int BEGINNING_OF_NAME = 2;
            std::string Name = Line.substr(Line.find_first_of(".")+BEGINNING_OF_NAME,Line.find_first_of(",")-(Line.find_first_of(".")+BEGINNING_OF_NAME));
            
            // Parse score from Line
            int32_t Score = std::stoi(Line.substr(Line.find_first_of(":")+1));
                        
            FScore TempScore(Name,Score);
            
            LoadedList.AddScore(TempScore);
        }

        //Sort the list
       SortHighscoreList(&LoadedList.HighscoreList);

    } else
    {
        std::cout<< "No file has been loaded!\n";
    }
    
    Storage.close();
    return LoadedList;
}
