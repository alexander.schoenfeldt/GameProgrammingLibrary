//
//  FHighscoreList.h
//  GameProgrammingLibrary
//
//  Created by Alexander Schoenfeldt on 20/08/16.
//  Copyright © 2016 Alexander Schoenfeldt. All rights reserved.
//
// This class provides a basic data structure to implement a score system in a game.

#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>

// Struct representing a single score entry in the highscore list
struct FScore {
public:
    
    std::string GetName() {return Name;}
    int GetScore(){return Score;}
    
    FScore(std::string Name, int Score)
    {
        this->Name = Name;
        this->Score = Score;
    };
    
   // < Operator overloading for sort algorithm
   friend bool operator<(const FScore &S1, const FScore &S2)
    {
        return S1.Score<S2.Score;
    }
    
    // > Operator overloading for sort algorithm
    friend bool operator>(const FScore &S1, const FScore &S2)
    {
        return S1.Score>S2.Score;
    }
    
private:
    std::string Name = "";
    int Score = 0;
};

class FHighscoreList
{
public:
    std::vector<FScore> GetHighscoreList();

    // If Index is out of bounds, an invalid score entry will be returned
    FScore GetHighscoreEntry(std::int32_t Index);
    
    // Also sorts the score into the right position
    void AddScore(FScore Score);
    
    void SortHighscoreList(std::vector<FScore> *HighscoreList);
    
    void SaveHighscoreList();
    
    FHighscoreList LoadHighscoreList(std::string Path);
    
private:
    std::vector<FScore> HighscoreList;
};